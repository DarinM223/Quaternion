extern crate vecmath;
extern crate quaternion;

use std::ops::Mul;

/// A four-tuple of real numbers [s, x, y, z] used to
/// represent both an orientation and a rotation
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Quaternion {
    pub s: f64,
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Quaternion {
    /// Normalizes a Quaternion
    pub fn norm(&self) -> f64 {
        (self.s * self.s + self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }

    /// Creates a new rotation quaternion given an angle an the axis vector
    pub fn rotation_quaternion(angle: f64, axis: (f64, f64, f64)) -> Quaternion {
        let q = Quaternion {
            s: (angle / 2.).cos(),
            x: (angle / 2.).sin() * axis.0,
            y: (angle / 2.).sin() * axis.1,
            z: (angle / 2.).sin() * axis.2,
        };
        let norm = q.norm();

        Quaternion {
            s: q.s / norm,
            x: q.x / norm,
            y: q.y / norm,
            z: q.z / norm,
        }
    }

    /// Returns the inverse of the Quaternion
    pub fn inverse(&self) -> Quaternion {
        let denom = self.s * self.s + self.x * self.x + self.y * self.y + self.z * self.z;

        Quaternion {
            s: self.s / denom,
            x: -self.x / denom,
            y: -self.y / denom,
            z: -self.z / denom,
        }
    }
}

/// Implements Quaternion multiplication
impl Mul<Quaternion> for Quaternion {
    type Output = Quaternion;

    /// Calculates the multiplication between two Quaternions
    fn mul(self, other: Quaternion) -> Quaternion {
        let cross_prod = cross_product(&self, &other);

        Quaternion {
            s: (self.s * other.s) - (self.x * other.x) - (self.y * other.y) - (self.z * other.z),
            x: (self.s * other.x) + (other.s * self.x) + cross_prod.0,
            y: (self.s * other.y) + (other.s * self.y) + cross_prod.1,
            z: (self.s * other.z) + (other.s * self.z) + cross_prod.2,
        }
    }
}

/// Computes the cross product between two Quaternions and returns a tuple 
/// of the x, y, and z components
pub fn cross_product(q1: &Quaternion, q2: &Quaternion) -> (f64, f64, f64) {
    (q1.y * q2.z - q1.z * q2.y, q1.z * q2.x - q1.x * q2.z, q1.x * q2.y - q1.y * q2.x)
}

fn main() {
    let q = Quaternion { s: 8., x: 1., y: 2., z: 3. };
    let q_inverse = q.inverse();

    println!("The inverse of q is: {:?}", q_inverse);
}

/// Test suite
#[cfg(test)]
mod tests {
    use std::f64::consts;
    use quaternion::{axis_angle, mul, rotate_vector};
    use vecmath::Vector3;

    use super::*;

    #[test]
    fn test_multiply() {
        let p = Quaternion { s: 3., x: 2., y: 5., z: 4. };
        let q = Quaternion { s: 4., x: 5., y: 3., z: 1. };

        // r = pq = [-17 -16 47 0]
        assert_eq!(p * q, Quaternion { s: -17., x: 16., y: 47., z: 0. });
    }

    #[test]
    fn test_inverse() {
        let q = Quaternion { s: 8., x: -2., y: 3., z: -1. };
        let q_inverse = q.inverse();

        // The quaternion times the inverse is equal to [1 0 0 0]
        assert_eq!(q * q_inverse, Quaternion { s: 1., x: 0., y: 0., z: 0. });
    }

    #[test]
    fn test_rotation_quaternion() {
        let rot_quat = Quaternion::rotation_quaternion(consts::PI / 6., (0., 0., 1.));
        let q = Quaternion { s: 0., x: 1., y: 0., z: 0. };

        // The rotated quaternion is the rotation quaternion times the original quaternion times
        // the inverse of the rotation quaternion
        let rotated_quat = rot_quat * q * rot_quat.inverse();
        assert_eq!(rotated_quat, Quaternion { s: 0., x: 0.8660254037844387, y: 0.49999999999999994, z: 0. });
    }

    #[test]
    fn test_rotation_part_d() {
        let rot_quat = Quaternion::rotation_quaternion(consts::PI / 4., (1., 1., 1.));
        let q = Quaternion { s: 0., x: 1., y: 2., z: 3. };
        let x_axis = Quaternion { s: 0., x: 1., y: 0., z: 0. };
        let y_axis = Quaternion { s: 0., x: 0., y: 1., z: 0. };
        let z_axis = Quaternion { s: 0., x: 0., y: 0., z: 1. };

        let rotated_quat = rot_quat * q * rot_quat.inverse();
        let rotated_x = rot_quat * x_axis * rot_quat.inverse();
        let rotated_y = rot_quat * y_axis * rot_quat.inverse();
        let rotated_z = rot_quat * z_axis * rot_quat.inverse();
        println!("Rotated quaternion: {:?}", rotated_quat);
        println!("Rotated x axis: {:?}", rotated_x);
        println!("Rotated x axis: {:?}", rotated_y);
        println!("Rotated x axis: {:?}", rotated_z);
    }

    #[test]
    fn test_rotation_part_e() {
        // Tests that the inverse rotation yields the same result as the normal rotation
        let q = Quaternion::rotation_quaternion(consts::PI / 3., (1., 1., 1.));
        let p = Quaternion { s: -q.s, x: -q.x, y: -q.y, z: -q.z };
        
        let x_axis = Quaternion { s: 0., x: 1., y: 0., z: 0. };
        let y_axis = Quaternion { s: 0., x: 0., y: 1., z: 0. };
        let z_axis = Quaternion { s: 0., x: 0., y: 0., z: 1. };

        println!("e Rotated x axis: {:?}", q * x_axis * q.inverse());
        println!("e Rotated y axis: {:?}", q * y_axis * q.inverse());
        println!("e Rotated z axis: {:?}", q * z_axis * q.inverse());

        assert_eq!(q * x_axis * q.inverse(), p * x_axis * p.inverse());
        assert_eq!(q * y_axis * q.inverse(), p * y_axis * p.inverse());
        assert_eq!(q * z_axis * q.inverse(), p * z_axis * p.inverse());
    }
}
